const express = require('express');
const user_file = require('./user.json');
const bodyParser = require('body-parser');
const app = express();
var requestJSON = require('request-json');
const port = process.env.PORT || 3000;
const URL_BASE = "/techu-peru/v1/";
app.use(bodyParser.json());
const controllers = require('./controllers/users');

var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu58db/collections/'
const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF'

// // peticion GET
// app.get(URL_BASE + 'users', function (req, res) {
//   //res.send ({"msj":"Operacion GET exitosa"});
// //  res.status(201);
//   res.status(200).send(user_file);
// });

//peticion GET users con ID
app.get(URL_BASE + 'users/:id', controllers.getUsers);

// app.get(URLbase + 'users/:id/accounts', account_controller.getAccounts);
//
// module.exports.getAccounts = getAccounts;


// //GET users con Query String
// app.get(URL_BASE + 'users',
// function(req, res){
// console.log('Get con Query String');
// console.log(req.query.id);
// console.log(req.query.name);
// });

// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users/:id consumiendo API REST de mLab
app.get(URL_BASE + 'users/mlab/:id',
 function(req, res) {
   console.log("GET /colapi/v3/users/:id");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryStrField = 'f={"_id":0}&';
   var id = req.params.id;
   var queryString = 'q={"id":' + id +'}&' + queryStrField;
   httpClient.get('user?' + queryString + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users/:id/accounts consumiendo API REST de mLab
app.get(URL_BASE + 'users/mlab/:id/accounts',
 function(req, res) {
   console.log("request.params.id: " + req.params.id);
   var id = req.params.id;
   var queryStringID = 'q={"id_user":' + id +'}&';
   var queryString = 'f={"_id":0}&';
   var httpClient = requestJSON.createClient(baseMLabURL);

   httpClient.get('account?' + queryString + queryStringID + apikeyMLab,
     function(err, respuestaMLab, body) {
       console.log('error ' + err);
       console.log('respuestaMLab ' + respuestaMLab);
       console.log('body ' + body);
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

//POST users
app.post(URL_BASE + 'users',
function(req, res){
  console.log('POST de users');
  // console.log('Nuevo usuario: ' + req.body);
  // console.log('Nuevo usuario: ' + req.body.first_name);
  // console.log('Nuevo usuario: ' + req.body.email);
  let newID = user_file.length + 1;
  let newUser = {
    "id": newID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  }
  user_file.push(newUser);
  console.log("nuevo usuario: " + newUser);
  res.send(newUser);
});

//POST users mLab
app.post(URL_BASE + 'users/mlab',
function(req, res){
  var clienteMLab = requestJSON.createClient(baseMLabURL);
  console.log(req.body);
  clienteMLab.get('user?' + apikeyMLab,
  function(error, requestMLab, body){
   newID = body.length + 1;
   console.log("newID:" + newID);
   var newUser = {
    "id": newID,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };
  // o clienteMLab.post(baseMLabURL + 'user?' + apikeyMLab, newUser,
  clienteMLab.post('user?' + apikeyMLab, newUser,
  function(error, requestMLab, body){
   console.log(body);
   res.status(201);
   res.send(body);
   });
 });
});


//PUT users
app.put(URL_BASE + 'users/:id',
function(req, res){
  console.log('PUT de users');
  let newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
    }
  user_file[req.params.id - 1] = newUser;
  console.log("usuario modificado: " + newUser);
  res.send(newUser);
});

//PUT users con parámetro 'id'
app.put(URL_BASE + 'users/mlab/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"id":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('user?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    clienteMlab.put(baseMLabURL +'user?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("body:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
      res.send(body);
     });
   });
});

// Petición PUT con id de mLab (_id.$oid)
 app.put(URL_BASE + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"id":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('user?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "id" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);          // PUT a mLab
         httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });

//DELETE users
app.delete(URL_BASE + 'users/:id',
function(req, res){
  console.log('Delete de users.');
  let pos = req.params.id;
  user_file.splice(pos - 1, 1);
  res.send({"msg":"usuario eliminado"});
});

// //Login de users
//  app.post(URL_BASE + 'login',
//  function(req, res) {
//  console.log('login');
//  console.log(req.body.email);
//  console.log(req.body.password);
//  let tam = user_file.length;
//  let i = 0;
//  let encontrado = false;
//  while ((i < user_file.length) && !encontrado){
//    if(user_file[i].email == email && user_file[i].password == password)
//    {
//      encontrado = true;
//      user_file[i].logged = true;
//    }
//    i++;
//  }
//  if (encontrado)
//  res.send({"encontrado":"si","id":i})
//  else {
//    res.send({"encontrato":"no"})
//  }
//  });
//
//
//
//  function writeUserDataToFile(data) {
//     var fs = require('fs');
//     var jsonUserData = JSON.stringify(data);
//     fs.writeFile("./users.json", jsonUserData, "utf8",
//      function(err) { //función manejadora para gestionar errores de escritura
//        if(err) {
//          console.log(err);
//        } else {
//          console.log("Datos escritos en 'users.json'.");
//        }
//      })
//   }

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
